default:
		@echo "what...???"

www: default

symfony: default
		/usr/sbin/a2dissite 000-default.conf
		/usr/sbin/a2dissite 002-wordpress.conf
		/usr/sbin/a2ensite 001-symfony.conf

wordpress: default
		/usr/sbin/a2dissite 000-default.conf
		/usr/sbin/a2dissite 001-symfony.conf
		/usr/sbin/a2ensite 002-wordpress.conf
